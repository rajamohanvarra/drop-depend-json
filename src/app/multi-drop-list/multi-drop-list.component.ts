import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AppSettingsService } from '../app-settings.service';



@Component({
  selector: 'app-multi-drop-list',
  templateUrl: './multi-drop-list.component.html',
  styleUrls: ['./multi-drop-list.component.css']
})
export class MultiDropListComponent implements OnInit {

  // private map = new Map<string, string[], string[]>([
  //   ['India',['Telengana',['Warszawa', 'Krakow']],'Andhra pradesh'],
  //   ['Poland', ['Warszawa', 'Krakow']],
  //   ['USA', ['New York', 'Austin']],
  // ])

  country: string;
  state: string;
  city: string;
  tables: any = {
    rows: [],
  cols: []
};

  // get countries(): string[] {
  //   return Array.from(this.map.keys());
  // }

  // get cities(): string[] | undefined {
  //   return this.map.get(this.country);
  // }
  initialInfo: any[] = [];
  countrySat: number;
  stateSat: number;
  citySat: number;
selectInfo: any[] = [];
 countryInfo: any[] = [];
 stateInfo: any[] = [];
 cityInfo: any[] = [];
 getIt = false;
  posts: any;
  constructor(
    private appSettingsService : AppSettingsService 
) { }

ngOnInit(){
  this.getCountries();
  this.getTable();
}
 
getCountries() {
  this.appSettingsService.getAllCountries().subscribe(
    data => {
      this.countryInfo = data.Countries;
      console.log(this.countryInfo);
      this.posts = this.countryInfo.slice(0, 1);
      console.log(this.posts)
    }
  )
}

onCountryChange(countryValue) {
  console.log(countryValue)
  this.countrySat = countryValue;
  this.stateInfo = this.countryInfo[countryValue].States;
  console.log(this.stateInfo)
  //this.cityInfo = this.stateInfo[0].cities;

  // this.selectInfo.push(this.countryInfo[countryValue].CountryName);
  // console.log(this.countryInfo[countryValue].CountryName)
  // console.log(`sel: ${this.selectInfo}`)


}

onStateChange(stateValue) {
  this.stateSat = stateValue;
  //console.log(`state val:${stateValue}`)
  this.cityInfo = this.stateInfo[stateValue].Cities;
  // console.log(this.stateInfo[stateValue].StateName)
  // this.selectInfo.push(this.stateInfo[stateValue].StateName)
  // console.log(`sel: ${this.selectInfo}`)
}

onCityChange(cityValue) {
 this.citySat = cityValue;
}

onShow() {

  if(this.countryInfo[this.countrySat] === undefined){
    this.getIt = false;
    alert('Please select country!')

  } else {
    this.getIt = true;
    // console.log(this.countryInfo)
    // console.log(this.stateInfo)
    // console.log(this.cityInfo)
    this.country = this.countryInfo[this.countrySat].CountryName;
    this.state = this.stateInfo[this.stateSat].StateName;
    this.city = this.cityInfo[this.citySat];
  }


  // this.selectInfo.push(this.stateInfo[this.stateSat].StateName);
  // console.log(this.countryInfo[this.countrySat].CountryName)

  console.log(`c: ${this.countrySat}: ${this.country}, s: ${this.stateSat}: ${this.state}, cc:${this.citySat}: ${this.city}`);

//   this.selectInfo.push(this.countryInfo[this.countrySat].CountryName, this.stateInfo[this.stateSat].StateName)
// console.log(this.selectInfo)


}

getTable(){
  // this.appSettingsService.getAllCountries().subscribe(
  //   data => {
      
  //     data.Countries.map(d => {
  //       const rowKeys = Object.keys(d);
  //       console.log(rowKeys)
  //       rowKeys.forEach(rowKey => {
  //         if (this.tables.cols.indexOf(rowKey) < 0) {
  //           this.tables.cols.push(rowKey);
  //           console.log(rowKey)
            
  //         }
  //       });

  //     });
  //     this.tables.rows = data.Countries;
  //   }
  // )
}

onScrollDown() {
  console.log('scrolled down!!');
  if(this.posts.length < this.countryInfo.length){  
    let len = this.posts.length;

    for(let i = len; i <= len+1; i++){
      this.posts.push(this.countryInfo[i]);
    }
    
  }
}

onScrollUp() {
  console.log('scrolled up!!');
}
  
}
