import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MultiDropListComponent } from './multi-drop-list.component';

describe('MultiDropListComponent', () => {
  let component: MultiDropListComponent;
  let fixture: ComponentFixture<MultiDropListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MultiDropListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MultiDropListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
